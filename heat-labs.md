# Basic Infrastructure Orchestration

These are step-by-step instructions for creating your own infrastructure in
SkyHiGh by using [predefined monolithic (all-in-one-file) Heat Orchestration
Templates](https://gitlab.com/erikhje/heat-mono) (sometimes called HOT
templates). The GUI you log in to at
[skyhigh.iik.ntnu.no](https://skyhigh.iik.ntnu.no) (only accessible from inside
NTNU or by [using
VPN](https://innsida.ntnu.no/wiki/-/wiki/Norsk/Installere+VPN)) is the OpenStack
component called Horizon (which is the web interface that talks to the OpenStack
API on your behalf) and when you choose "Orchestration" in this GUI you are
using Horizon to talk to the OpenStack component Heat (which is the
orchestration component of OpenStack).

## Launching an Infrastructure

1. Log in (NTNU credentials) at <https://skyhigh.iik.ntnu.no>
1. (if teacher says that you have a service account and should use it, click on "Object store", "Containers", "servicepassword", and "Download". Log out and log back in using your service account username and password, and choose "Openstack accounts" instead of "NTNU accounts")
1. Create a keypair unless you already have one:
   1. Compute
   1. Key Pairs
   1. Create Key Pair
   1. Key Pair Name `mykey` (or whatever name you prefer)
   1. Create Key Pair. **This will output a file mykey.pem which is a very
      important file containing your private key, make sure you have control
      over where this file is stored.** The corresponding public key is stored
      in OpenStack and used to generate passwords in Windows instances, and
      injected in Linux instances for key-based authentication.
1. Create your infrastructure:
   1. Orchestration
   1. Stacks
   1. Launch Stack
   1. Template Source: Direct Input
   1. Template Data: copy and paste the entire content from [your preferred
      yaml-file](https://gitlab.com/erikhje/heat-mono) 
   1. (You do not have to choose “Environment Source” or
				“Environment File”)
   1. Stack Name: `Mysil` (or whatever name you prefer)
   1. Enter your NTNU-password
   1. key\_name: `mykey` (or whatever you chose as the name in the step earlier)
   1. Launch
1. Wait 5-15 minutes for Windows instances, 1-5 minutes for Linux instances. 
1. If you would like, you can monitor progress for each instance by clicking
   Network, Network Toplogy, choose an instance, Console
1. Click on Compute, Instances and make a note of the IP addresses starting with
   `10.212`. Those are the floatingIPs which are publically available (but
   restricted to access only from NTNU networks) and you use when accessing your
   instances with RDP or SSH.

## Accessing Windows Instances

1. Your username is `Admin`. Find your password:
   1. Compute
   1. Instances
   1. Retrieve Password (right side drop-down menu)
   1. Private Key File: upload `mykey.pem` (or copy and paste its contents)
   1. Decrypt Password
   1. Your password will be displayed
1. To connect to a Windows instance, use a remote desktop client application
   (`mstsc` on Windows, `Microsoft Remote Desktop connection` from App store on
   Mac, or `xfreerdp` on Linux)
<!--
1. Before you start using a new server, better make sure all
software is updated:
```powershell
# PowerShell -> Run as administrator 
Set-ExecutionPolicy RemoteSigned
# Oppdater
Install-Module -Name PSWindowsUpdate
Get-WindowsUpdate 
Get-WindowsUpdate -Download
Get-WindowsUpdate -Install
```
-->

## Accessing Linux Instances

Your username is `ubuntu`. There is no password, use your private key (the file
`mykey.pem`) to log in:

```console
ssh -i mykey.pem ubuntu@FLOATING_IP_ADDRESS
```

If on Mac or Linux, you probably need to do `chmod 600 mykey.pem` first.

On Windows, you probably need to do in PowerShell

```console
cmd
icacls .\mykey.pem /inheritance:r
icacls .\mykey.pem /grant:r "%username%":"(R)"
exit
```

If this does not work (still gives error "WARNING: UNPROTECTED PRIVATE KEY
FILE!") then fix with File explorer:
   a. navigate to the pem-file
   b. right click the file
   c. choose properties, security and remove all "Group or user names" other than yourself
   d. apply, ok

## Advanced: Automating Access to OpenStack

If you would like to run commands in OpenStack using the command line client you
need to authenticate, and to avoid putting your NTNU credentials in any file or
script, it is recommended to use [Application
Credentials](https://www.ntnu.no/wiki/display/skyhigh/Application+credentials).