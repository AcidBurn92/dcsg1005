# Introduction

### When the Shit Hits the Fan...

![image](../toten.jpg)

  - How long did they have access?

  - Are they out now?

  - Are the systems/backups "clean"?

### Introduction

Two categories of data:

  - Counters (numeric values)

  - Log events (text messages)

<span class="alert">Sometimes log events are generated from
counters</span>

# Counters

## Terminology

### Terminology

  - CounterSet  
    e.g. `Process`

  - Counter  
    e.g `Working Set`

  - Instance  
    e.g. `pwsh#1` (instances are numbered if there are more than one)

  - Path  
    e.g. `\Process(pwsh#1)\Working Set`

A special instance <span class="alert">\_Total</span>

Windows contains a subsystem known as Performance Logging and Alerting
(PLA). It contains *counter sets* with *counters*. A counter can be
*single-instance* (a single value) or *multi-instance* (e.g. one for
each CPU and one called `_total`). A counter is specified with a *path*.

## Implementation

### Implementation

  - `Get-Counter`

  - `Get-CimInstance` (CIM - Common Information Model, in Windows: WMI
    Windows Management Instrumentation)

  - Use .NET directly
    
    ``` 
          New-Object System.Diagnostics.PerformanceCounter
           ("Processor Information", "% Processor Time",
            "_Total")
    ```

<!-- end list -->

  - Get-Counter  
    e.g. `(Get-Counter -ListSet Processor).Counter`

  - Get-CimInstance  
    “Windows Management Instrumentation (WMI) consists of a set of
    extensions to the Windows Driver Model that provides an operating
    system interface through which instrumented components provide
    information and notification. WMI is Microsoft’s implementation of
    the Web-Based Enterprise Management (WBEM) and Common Information
    Model (CIM) standards from the Distributed Management Task Force
    (DMTF). WMI allows scripting languages (such as VBScript or Windows
    PowerShell) to manage Microsoft Windows personal computers and
    servers, both locally and remotely.” 

  - PLA directly  
    no cmdlets, use .NET directly

### Overview

  - All CounterSets
    
    ``` 
        Get-Counter -ListSet * |
         Sort-Object CounterSetName | 
         Select-Object -Property CounterSetName |
         Out-GridView    
    ```

  - How many Counters are there?
    
    ``` 
        Get-Counter -ListSet * | 
         Select-Object -ExpandProperty Counter |
         Measure-Object
    ```

  - All Counters in a specific CounterSet
    
    ``` 
        Get-Counter -ListSet Process | 
         Select-Object -ExpandProperty Counter
    ```

We want the value called `CookedValue`. There is a raw value and a
secondary value as well, but these are combined into something that
makes sense to us, and that is the CookedValue.

### Getting the Values

  - Show the counter
    
    ``` 
      Get-Counter -Counter '\Process(_Total)\Working Set'
    ```

  - The CounterSamples-object
    
    ``` 
      (Get-Counter -Counter `
        '\Process(_Total)\Working Set').CounterSamples
    ```

  - The values
    
    ``` 
      (Get-Counter -MaxSamples 3 -Counter `
        '\Process(_Total)\Working Set').CounterSamples.CookedValue
    ```

### Retrieve Counters From Remote Hosts

``` 
  # Do this:
  $scriptblock = { 
    Get-Counter '\Processor(_total)\% Processor Time'
    }
  Invoke-Command -ComputerName dc1,srv1 `
                 -ScriptBlock $scriptblock

  # Do not do this:
  Get-Counter -ComputerName dc1,srv1 `
    '\Processor(_total)\% Processor Time'

  #
  Get-Command | 
    Where-Object { 
      $_.Parameters.Keys -contains "ComputerName" -and 
      $_.Parameters.Keys -notcontains "Session"}
```

### Exporting and Analyzing

``` 
  $counters = '\Processor(0)\% Processor Time',
              '\Process(_Total)\Working Set'

  # Export-Counter is only in Windows PowerShell (5.1)
  Get-Counter -Counter $counters -MaxSamples 10 |
   Export-Counter -Path C:\PerfLogs\cap.csv -FileFormat csv

  # Need to do this in PowerShell Core instead
  Get-Counter -Counter $Counters -MaxSamples 10 | 
  ForEach-Object {
    $_.CounterSamples | ForEach-Object {
      [pscustomobject]@{
        TimeStamp = $_.TimeStamp
        Path = $_.Path
        Value = $_.CookedValue
      }
    }
  } | Export-Csv -Path $home\out.csv -NoTypeInformation
    
  Get-Content out.csv | clip.exe
  # paste in Excel, Data, Text to Columns)
```

## GUI Tools

### GUI Tools

  - Task Manager

  - Performance Monitor

  - Resource Monitor

  - Windows Admin Center

  - (and a bunch of others)

## Home Exercise

### Home Exercise

<span class="alert">Try to do this before Thursday</span>:

> Write a PowerShell script or function `Get-IOPS` which outputs the sum
> of disk reads and writes for the last second. You can find these two
> Counters in the CounterSet `PhysicalDisk`.

See
[https://en.wikipedia.org/wiki/IOPS](https://en.wikipedia.org/w/index.php?title=IOPS&oldid=997276912)

# Log Events

### A Log on a Server

![image](../log.jpg)

## Terminology

### Terminology

  - Event provider  
    An application that generates events

  - Event  
    A log entry

  - Event log  
    A file containing events (Application, Security, System plus many
    app specific)

  - Event type  
    Critical, Error, Warning, Information, Verbose, Debug, Success
    Audit, Failure Audit

  - Event ID  
    A number representing specific event (<span class="alert">only
    unique per source/provider</span>)

A log entry is composed of many fields. Unfortunately there is no
standard for log entries, but most of the time there are some fields
that appear to be quite standard such as timestamp, hostname,
processname/source and message. On Windows, there is also the event-ID
and event-type.

Some event logs (categories) like Application, Security and System
receive event from many sources while many applications have their own
event logs, e.g. ’Microsoft-Windows-WindowsUpdateClient/Operational’

## Log Mode

### Log Mode

Eventlogs can be in [one of three
different](https://docs.microsoft.com/en-us/dotnet/api/system.diagnostics.eventing.reader.eventlogmode)
`EventLogMode`

  - AutoBackup (`1`)  
    Archive the log when full, do not overwrite events. The log is
    automatically archived when necessary. No events are overwritten.

  - Circular (`0`)  
    New events continue to be stored when the log file is full. Each new
    incoming event replaces the oldest event in the log.

  - Retain (`2`)  
    Do not overwrite events. Clear the log manually rather than
    automatically.

`Get-WinEvent -ListLog * | Format-Table -Property LogName,LogMode`

## Log files

### Where are the eventlogs/files?

    # How many logfiles?
    Get-ChildItem C:\Windows\System32\winevt\Logs\ | 
     Measure-Object
    
    # How many of each size?
    Get-ChildItem C:\Windows\System32\winevt\Logs\ | 
       Group-Object -Property Length
    
    # Which one most recently written to?
    Get-ChildItem C:\Windows\System32\winevt\Logs\ | 
     Sort-Object -Property LastWriteTime

The log files are managed and written to by the EventLog service which
several other services depend on.

    PS> (Get-Service EventLog).DependentServices
    
    Status   Name               DisplayName
    ------   ----               -----------
    Stopped  Wecsvc             Windows Event Collector
    Stopped  NcdAutoSetup       Network Connected Devices Auto-Setup
    Stopped  AppVClient         Microsoft App-V Client
    Running  netprofm           Network List Service
    Running  NlaSvc             Network Location Awareness

## Three cmdlets

There are three approaches in PowerShell to retrieving log events:
`Get-EventLog`, `Get-WinEvent` and `Get-CimInstance Win32_NTLogEvent`.
Sometimes it faster and/or easier to use one or the other. The newest
one and probably the one you would try first is `Get-WinEvent` since it
supports retrieving all logs as opposed to the others:

    # Only in Windows PowerShell (5.1)
      (Get-EventLog -LogName * | Measure-Object).Count
    
      (Get-WinEvent -ListLog * | 
         Where-Object {$_.RecordCount -gt 0} | Measure-Object).Count
    
      (Get-CimInstance Win32_NTLogEvent | 
         Select-Object -Property Logfile | 
          Sort-Object -Property Logfile | 
           Get-Unique -AsString | Measure-Object).Count

`Get-WinEvent` is the recommended cmdlet to use in PowerShell Core.

The three approaches return different types of objects:

| Cmdlet                             | Type                                              |
| :--------------------------------- | :------------------------------------------------ |
| `Get-EventLog`                     | System.Diagnostics.EventLogEntry                  |
| `Get-WinEvent`                     | System.Diagnostics.Eventing.Reader.EventLogRecord |
| `Get-CimInstance Win32_NTLogEvent` | Microsoft.Management.Infrastructure.CimInstance   |

which means differences in property-names as well, so `Get-Member` and
`Select-Object -Property *` comes in handy to understand the
differences. E.g. if you want to retrieve a specific log entry:

### Three cmdlets

    Get-EventLog -LogName Security | 
      Where-Object {$_.Index -eq 20905}
    
    Get-WinEvent -LogName Security | 
      Where-Object {$_.RecordId -eq 20905}
    
    Get-CimInstance Win32_NTLogEvent | 
      Where-Object {$_.RecordNumber -eq 20905}

Here is a overview of differences in the most important property names:

| Field      | Get-EventLog    | Get-WinEvent       | Get-CimInstance |
| :--------- | :-------------- | :----------------- | :-------------- |
| Time stamp | `TimeGenerated` | `TimeCreated`      | `TimeGenerated` |
| Host       | `MachineName`   | `MachineName`      | (missing)       |
| Source     | `Source`        | `ProviderName`     | `SourceName`    |
| Message    | `Message`       | `Message`          | `Message`       |
| Type       | `EntryType`     | `LevelDisplayName` | `Type`          |
| Event ID   | `InstanceId`    | `Id`               | `EventCode`     |

### Get-EventLog

    Get-EventLog -LogName Security | 
     Select-Object -First 1 | 
      Get-Member | 
       Where-Object {$_.MemberType -eq 'Property'} | 
        Format-Table -Property Name
    
    TypeName: System.Diagnostics.EventLogEntry#Security/Microsoft-Windows-Security-Auditing/4624
    
    Name
    ----
    Category
    CategoryNumber
    Container
    Data
    EntryType
    Index
    InstanceId
    MachineName
    Message
    ReplacementStrings
    Site
    Source
    TimeGenerated
    TimeWritten
    UserName

### Get-WinEvent

    Get-WinEvent -LogName Security | 
     Select-Object -First 1 | 
      Get-Member | 
       Where-Object {$_.MemberType -eq 'Property'} | 
        Format-Table -Property Name
    
    TypeName: System.Diagnostics.Eventing.Reader.EventLogRecord
    
    Name
    ----
    ActivityId
    Bookmark
    ContainerLog
    Id
    Keywords
    KeywordsDisplayNames
    Level
    LevelDisplayName
    LogName
    MachineName
    MatchedQueryIds
    Opcode
    OpcodeDisplayName
    ProcessId
    Properties
    ProviderId
    ProviderName
    Qualifiers
    RecordId
    RelatedActivityId
    Task
    TaskDisplayName
    ThreadId
    TimeCreated
    UserId
    Version

### Get-CimInstance

    Get-CimInstance Win32_NTLogEvent | 
     Select-Object -First 1 | 
      Get-Member | 
       Where-Object {$_.MemberType -eq 'Property'} | 
        Format-Table -Property Name
    
    TypeName: Microsoft.Management.Infrastructure.CimInstance#root/cimv2/Win32_NTLogEvent
    
    Name
    ----
    Category
    CategoryString
    ComputerName
    Data
    EventCode
    EventIdentifier
    EventType
    InsertionStrings
    Logfile
    Message
    PSComputerName
    RecordNumber
    SourceName
    TimeGenerated
    TimeWritten
    Type
    User

## Using Get-WinEvent

### Most Recent

### Most Recent in Common Logs

``` 
  Get-WinEvent -MaxEvents 10 -LogName `
    Application,System,Security,"Windows PowerShell" |
    Format-Table -Property `
     LogName,TimeCreated,ID,LevelDisplayName,Message
```

### Specific EventIDs

### Specific EventIDs

    # Note the use of 'ExpandProperty'
    # to see entire message
    
    Get-WinEvent  -MaxEvents 10 -FilterHashtable `
      @{ LogName='Security'; Id='4672','4624' } |
      Select-Object -Last 1 -ExpandProperty Message

### Levels

|               |   |
| :------------ | :- |
| Verbose       | 5 |
| Informational | 4 |
| Warning       | 3 |
| Error         | 2 |
| Critical      | 1 |
| LogAlways     | 0 |

### Search all

### Query All Logs

    # This fails due to 256 max
    Get-WinEvent -MaxEvents 10
    
    $logs = (Get-WinEvent -ListLog * |
      Where-Object {$_.RecordCount} | 
      Select-Object -ExpandProperty Logname)
    
    # 25 most recent Warning and Information
    Get-WinEvent -MaxEvents 25 `
      -FilterHashtable @{Logname=$logs;Level=3,4}

### Ignore

### Ignore "Information" level

    $Date = (Get-Date).AddDays(-2)
    
    # Ignore "Information" by suppress level 4
    $filter = @{
      LogName='Application'
      StartTime=$Date
      SuppressHashFilter=@{Level=4}
    }
    
    Get-WinEvent -FilterHashtable $filter

### Time period

### Time Period

Remember *Filter left, Format right*

    $start = (Get-Date).AddDays(-1)
    $end   = Get-Date
    
    # DO THIS
    Get-WinEvent -FilterHashtable `
     @{ logname="System"; starttime=$start; endtime=$end }
    
    # DO NOT DO THIS
    Get-WinEvent -LogName System | 
     Where-Object { $_.TimeCreated -gt $start `
      -and $_.TimeCreated -lt $end }

# Monitoring

### For Operations

Start your monitoring from users perspective: "Test if product can be
bought in the webshop"

### For Security

Read post-mortems, best practice, analysis, etc on what to look for, a
few tips:

  - See Event IDs that matter at <https://adsecurity.org/?p=3299>

  - [www.malwarearchaeology.com/cheat-sheets](www.malwarearchaeology.com/cheat-sheets)

  - [github.com/SwiftOnSecurity/sysmon-config](github.com/SwiftOnSecurity/sysmon-config)

  - [Threat Detection with Windows Event
    Logs](https://medium.com/adarma-tech-blog/threat-detection-with-windows-event-logs-59548f4f7b85)

If you find a resource which has a list of event IDs that you want to
search your logs for, you can copy and paste the list into a file e.g.
`a.txt` and extract all the event IDs into an array like this

    $IDs = Select-String -Pattern '\d{4}' .\a.txt -AllMatches |
     Select-Object -ExpandProperty Matches |
      Select-Object -Property Value
