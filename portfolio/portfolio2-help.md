1.  **Allowing users to RDP** By default only members of the
    Adminstrators group can RDP to workstations, servers and domain
    controllers. Allowing others access should be done carefully, and
    it’s a good rule to always look for official docs with best
    practice such as [Allow log on through Remote Desktop
    Services](https://docs.microsoft.com/en-us/windows/security/threat-protection/security-policy-settings/allow-log-on-through-remote-desktop-services).
    Let us follow this document and create an ordinary user `mysil` who
    is allowed to RDP to CL1 and SRV1. Log in as Domain Administrator on
    DC1.
    
    1.  Create OUs for workstations (CL1) and servers (SRV1 and SRV2)
        and move the objects in AD
        
            New-ADOrganizationalUnit -Name "WORKSTATIONS" -Path "DC=sec,DC=core"
            New-ADOrganizationalUnit -Name "SERVERS" -Path "DC=sec,DC=core"
            Move-ADObject -Identity "CN=CL1,CN=COMPUTERS,DC=sec,DC=core" `
             -TargetPath "OU=WORKSTATIONS,DC=sec,DC=core"
            Move-ADObject -Identity "CN=SRV1,CN=COMPUTERS,DC=sec,DC=core" `
             -TargetPath "OU=SERVERS,DC=sec,DC=core"
    
    2.  Create an OU for "strange users" called MISC and create mysil in
        it, provide a complex password (write it down)
        
            New-ADOrganizationalUnit -Name "MISC" -Path "DC=sec,DC=core"
            $USER = @{
             Name                  = "Mysil Bergsprekken"
             GivenName             = "Mysil"
             Surname               = "Bergsprekken" 
             SamAccountName        = "mysil" 
             UserPrincipalName     = "mysil@reskit.org" 
             Path                  = "OU=MISC,DC=sec,DC=core"
             Enabled               = $true
             PasswordNeverExpires  = $true;
             ChangePasswordAtLogon = $false
            }
            New-ADUser @USER -AccountPassword(Read-Host -AsSecureString `
                                              "Input Password")
            
            # btw, remove a user with
            #Get-ADuser mysil | Remove-ADUser
            
            # and verify a user/passwork combination with
            #$username ='RESKIT\mysil'
            #$password = 'YOUR_CHOSEN_PASSWORD'
            #$computer = $env:COMPUTERNAME
            #Add-Type -AssemblyName System.DirectoryServices.AccountManagement
            #$obj = New-Object System.DirectoryServices.AccountManagement.
                               PrincipalContext('machine',$computer)
            #$obj.ValidateCredentials($username, $password)
            # This returns true if correct user/password combination
    
    3.  Let’s create a new group in AD "My RDP Users" which we can add
        to local hosts to allow RDP access
        
            $GROUP = @{
             Name          = "My RDP Users" 
             GroupCategory = "Security" 
             GroupScope    = "Global" 
             DisplayName   = "Local RDP users" 
             Path          = "DC=sec,DC=core" 
             Description   = "To be added locally with GPP"
            }
            New-ADGroup @GROUP
    
    4.  Add mysil to the group
        
            Get-ADGroupMember "My RDP Users"
            Add-ADGroupMember -Members mysil `
             -Identity 'CN=My RDP Users,DC=sec,DC=core'
            Get-ADGroupMember "My RDP Users"
    
    5.  For this to work, "My RDP Users" needs to be added to the local
        Remote Desktop users group on each host, we can do this
        centralized by using Group Policy Preferences (this is the
        recipe from [this answer](https://superuser.com/a/1182426) on
        StackExchange)
        
        1.  On DC1, start Group Policy Management Console (`gpmc.msc`)
        
        2.  Create a new Group Policy Object called `MyRDP` and navigate
            to  
            `Computer Configuration`, `Preferences`, `Control Panel
            Settings`
        
        3.  Right-click `Local Users and Groups` and choose `New`,
            `Local Group`
        
        4.  Set Action: to `Update`
        
        5.  In the Group name: drop-down choose `Remote Desktop Users
            (built-in)`
        
        6.  Click Add...
        
        7.  In the Local Group Member dialog box click the ... box and
            find your group `My RDP Users` (don’t type it in manually)
        
        8.  Confirm Action: is set to Add to this group
        
        9.  Click OK two times then close the Group Policy editor.
        
        10. Apply the Group Policy object to computers to which you want
            users to be able to access.
            
                Get-GPO -Name "MyRDP" |
                 New-GPLink -Target "OU=WORKSTATIONS,DC=sec,DC=core"
