# Software App

### What Is a Software Application?

  - <span class="alert">Executables</span>

  - Shared and non-shared libraries

  - Images (icons), sounds

  - Manual/Help files

  - Directories

  - Config files and registry/database entries

  - Menu entries and shortcuts

  - Environment variables

So we [create a <span class="alert">software package, e.g.
MSIX</span>](https://docs.microsoft.com/en-us/windows/msix/)

A software application is much more than just the executable file you
run to start the application.

## Licensing

### Licensing Issues

  - Open Source/Free software

  - EULA’s

  - Product keys

  - Dual licensing

  - Product activation

  - Time-restricted licenses

  - Per user/Per host licenses

  - License servers

  - <span class="alert">Keeping track of license use ...</span>

See e.g [Microsoft 365 License
plans](https://www.microsoft.com/nb-no/microsoft-365/compare-microsoft-365-enterprise-plans?market=no)
and [Azure Pricing
Calculator](https://azure.microsoft.com/en-us/pricing/calculator/)

You can make a lot of money just by being a software licensing expert
and doing consultancy services ...

## Framework

### We Don’t Want This\!

![image](../update-screenshot.png)

### Framework

  - Source  
    Source code compiled to binary <span class="alert">with
    dependencies</span>

  - Package  
    All files incl description of other
    <span class="alert">dependencies</span> zipped together in some
    standardized format

  - Remote Package Repo  
    A repository of packages somewhere on the Internet

  - Local Package Repo  
    Company internal repository of packages mostly for
    <span class="alert">testing</span>

  - Download/Install Mechanism  
    Install software packages

  - Ready Software Application  
    Client software (chrome, edge, ...) and server software (Exchange,
    nginx, ...)

Make drawing...

# Updates

### Many kinds of Windows updates...

There are many kinds of update packages, search for “Windows update
formats” in [Raymond Chen’s blog “Old New
Thing”](https://devblogs.microsoft.com/oldnewthing) to learn about
full, delta, express and quality updates.

### Updates Fail

![image](../windows-update-problems.png)

### Fresh Install vs Update

  - Where are all the files to be updated?

  - What if the update fails?

  - <span class="alert">Why are updates different from fresh
    installs?</span>
    
      - no physical access required?
    
      - host may not be in a “known state”
    
      - host may have “live” users
    
      - host may be gone
    
      - host may be dual-boot

There has to be a way to locate the files installed from previous
versions of the package, and during an update process everything have to
be carefully backed up so it can be restored if something fails (e.g. a
power shutdown while copying files).

# Uninstall

### Uninstall and Transactional Behaviour

  - Installations/updates have to be transactional/atomic in behaviour

  - There should also be a transactional uninstall

  - <span class="alert">Should the uninstall remove the dependencies
    that were installed together with the original installation?</span>

Removing dependencies that were installed together with the original
installation is also sometimes called *cascading package removal*.

# Security

### Package Management Security

<span class="alert">Installations are performed with high
(root/administrator) privileges: We really need to trust the packages
(and the source they come from) we are installing\!</span>

### Supply Chain Attacks

  - event-stream (2018)  
    [Handing over responsibility for a widely used library to someone
    else...](https://github.com/dominictarr/event-stream/issues/116)

  - SolarWinds (2020)  
    ["They gained access to victims via trojanized updates to
    SolarWind’s Orion IT monitoring and management
    software"](https://www.fireeye.com/blog/threat-research/2020/12/evasive-attacker-leverages-solarwinds-supply-chain-compromises-with-sunburst-backdoor.html)

And of course, this also means using cryptographic hashes and digital
signatures, hashing and signing maybe repository index metadata (root
metadata), package metadata and the package data itself.

Samuel and Cappos  did an interesting study on security of package
managers back in 2009. These attacks probably does not work anymore, but
how to think about attacks on package management is still useful.

### Replay Attack

If the attacker is MITM, they can serve an old version of the repository
even though the root metadata is signed ...

<span class="alert">Protect by making sure you dont accept metadata
older than what you already have</span>.

### Freeze Attack

If the attacker is MITM, she can avoid updating the repository ...

<span class="alert">Protect by limiting how long signed root metadata is
valid</span>.

### Metadata Manipulation Attack

If metadata is not signed (not root nor package metadata), MITM can
easily offer newer versions of packages which are actually older version
(with vulnerabilities the attacker know how to exploit) ...

<span class="alert">Protect by requiring signed metadata</span>.

### Endless Data (DOS) Attack

As root metadata the MITM attacker will just serve an endless file ...

<span class="alert">Protect by monitoring system resources, setting hard
limits or possibly by keeping package management cache on a separate
partition</span>.

### Protection

<span class="alert">We must trust our repositories...</span>

  - Be aware of the software repositories you use and who maintains them

  - Maintain your own software repositories for your infrastructure

The problem is that its too easy to become a repository maintainer
(mirror a repository) for many software package systems, and as soon as
you are a mirror you can initiate the mentioned attacks if the
distribution is vulnerable.

# Internal repo

## WSUS

### Windows Server Update Services

[Deploy Windows Server Update
Services](https://docs.microsoft.com/en-us/windows-server/administration/windows-server-update-services/deploy/deploy-windows-server-update-services)

  - `Get-WsusUpdate`

  - `Get-WsusProduct`

  - `Get-WsusClassification`

  - `$WSUSServer.GetSubscription()`

  - `$WSUSSub.GetSynchronizationStatus()`

  - `$WSUSServer.GetComputerTargetGroups()`

  - `$WSUSServer.GetInstallApprovalRules()`

  - `Get-WsusComputer`

With WSUS, you don’t get access to everything you might need from the
cmdlets, so many times you will use the methods of the wsuserver-object
instead of cmdlets, e.g.

    $WSUSServer = Get-WsusServer
    $WSUSServer.GetConfiguration()

The downside to this is that these methods are not as well documented as
cmdlets.

There is another good PowerShell module for WSUS at
<https://github.com/proxb/PoshWSUS>

## Chocolatey

### Internal Chocolatey Repo

[See the Chocolatey
Architecture](https://docs.chocolatey.org/en-us/guides/organizations/automate-package-internalization#architecture)

# Installers

## PowerShell

### PackageManagement

PowerShell’s package management architecture

![image](../OneGetArchitecture.png)  
<span> from [PackageManagement (aka
OneGet)](https://github.com/oneget/oneget)</span>

    Get-Command -Module PackageManagement |
         Format-Table -Property Name

But unified package management is not easy, e.g updates are still
problematic with PowerShell’s high level framework. But it is a good
reference to understand the different packages and sources we have to
deal with on Windows. To try to get an overview we can list installed
packages in different ways and compare:

    # Ask Windows Management Instrumentation (WMI/CIM)
    Get-CimInstance Win32_Product | 
      Format-Table -Property Name,Version
    
    # Ask chocolatey
    choco list --local-only
    
    # Ask with PowerShell's PackageManagement
    Get-Package | Format-Table -Property Name,Version,Providername
    
    # Query registry, probably the best one to use combined with any 
    # third party package manager you use like chocolatey
    $registrylocations = (
      "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall",
      "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall",
      "HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall"
    )
    $registrylocations | Get-ChildItem | ForEach-Object {
      Write-Output "$($_.GetValue('DisplayName')) $($_.GetValue('DisplayVersion'))"
    } | Where-Object { $_.Trim() -ne '' }

The last example is inspired by [How to use PowerShell to PowerShell to
List installed
Software](https://adamtheautomator.com/powershell-list-installed-software/).

## Scoop

### Scoop

  - Installer  
    <https://scoop.sh>

  - Default repo  
    [https://github.com...](https://github.com/ScoopInstaller/Main/tree/master/bucket)

## Chocolatey

### Chocolatey

  - Installer  
    <https://chocolatey.org/install>

  - Default repo  
    <https://chocolatey.org/packages>

## AppGet

### AppGet

  - Installer  
    <https://appget.net/download>

  - Default repo  
    <https://appget.net/packages>

Microsoft probably got many ideas from AppGet when they created WinGet,
the story [The Day AppGet Died](https://keivan.io/the-day-appget-died/)
is an interesting read.

## WinGet

### WinGet

See [Use the winget tool to install and manage
applications](https://docs.microsoft.com/en-us/windows/package-manager/winget/)

  - Installer  
    [https://github.com...](https://github.com/microsoft/winget-cli/releases)

  - Default repo  
    <https://winget.azureedge.net/cache>

Will WinGet be the future?

## Ninite

### Ninite

Create a mix of packages, can be updated as a unit, see
[https://ninite.com](https://ninite.com/)

# SW Vulnerabilities

## Terminology

### Terminology

  - CVE  
    <span class="alert">Common Vulnerabilities and Exposures</span>  
    [https://cve.mitre.org](https://cve.mitre.org/)

  - (NVD)  
    (National Vulnerability Database)  
    [https://nvd.nist.gov/](https://nvd.nist.gov)

  - CVSS  
    <span class="alert">Common Vuln. Scoring System</span>  
    <https://www.cvedetails.com>

[NVD gets its data from
CVE](https://cve.mitre.org/about/cve_and_nvd_relationship.html).

# SW Threats

## Terminology

### Terminology

Mitre also has a nice overview for Attacks:  
[Windows Matrix](https://attack.mitre.org/matrices/enterprise/windows/)
